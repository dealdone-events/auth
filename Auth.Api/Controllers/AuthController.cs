using Microsoft.AspNetCore.Mvc;

namespace Auth.Controllers
{
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return Ok("Я - авторизация. Желаю тебе хорошего тебе дня!");
        }
    }
}
